<?php

/*
 * Лешуков О.Е. Тестирование ендпоинта.
 *
 */

class OptimaxTestingCest
{
    public string $responseId;

    // step 1: создать кастомера и вернуть его ид в бд
    public function createCustomer(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/customer', [
            'name' => 'Oleg',
            'firstname' => 'Ol',
            'lastname' => 'Lesh',
            'phone' => '+7(999) 876 54-32',
            'email' => 'aaaaa@bbbbb.com'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $responseJson = $I->grabResponse();
        $this->responseId = json_decode($responseJson)->customerId;
        // вставил эхо чтобы логировать результаты
        echo 'создан кастомер с ид ' . $this->responseId;
    }

    // step 2: получить кастомера по ид
    public function getCustomer(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/api/customer/' . $this->responseId
        );
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        echo ('Кастомер: ' . $I->grabResponse());
    }

    // step 3: получить всех кастомеров
    public function getAllCustomers(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/api/customers/'
        );
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        echo ('Кастомеры: ' . $I->grabResponse());
    }

    // step 4: обновить кастомера
    public function updateCustomer(\ApiTester $I)
    {
        $I->sendPut('/api/customer/' . $this->responseId . '?name=Antonio&firstname=Antonio&lastname=Banderas&phone=87264125312&email=antban@gmail.com'
        );
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        echo 'Результат обновления кастомера: ' . $I->grabResponse();
    }

    // step 5: удалить кастомера
    public function deleteCustomer(\ApiTester $I)
    {
        $I->sendDelete('/api/customer/' . $this->responseId);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        echo 'Результат удаления кастомера: ' . $I->grabResponse();
    }
}
